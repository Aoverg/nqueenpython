from asyncore import read
from operator import truediv

class Board:
    size = 8

def boardGenerator():
    gameBoard = []

    for i in range(Board.size):
        gameBoard.append([])
        for j in range(Board.size):
            gameBoard[i].append(0)

    return gameBoard


def printBoard(gameBoard):
    for i in gameBoard:
        for j in i:
            print(j, end='')
            print(" ", end='')
        print("")


def valid(gameBoard, y, x):
    row = 0
    col = 0
    upRight = 0
    upLeft = 0

    # Check collumn
    for i in range(Board.size):
        col += gameBoard[i][x]
        if (col > 1):
            return False

    # check row
    for i in range(Board.size):
        row += gameBoard[y][i]
        if (row > 1):
            return False

    # check upRight
    xpos = x
    ypos = y
    while (ypos >= 0 and xpos < Board.size):
        upRight += gameBoard[ypos][xpos]
        xpos += 1
        ypos -= 1
        if (upRight > 1):
            return False

    # check upLeft
    xpos = x
    ypos = y
    while (ypos >= 0 and xpos >= 0):
        upLeft += gameBoard[ypos][xpos]
        xpos -= 1
        ypos -= 1

        if (upLeft > 1):
            return False
    
    return True
	

def populateBoard(y, gameBoard):
	if(y >= Board.size):
		return True
	
	for x in range(Board.size):
		gameBoard[y][x] = 1
		if(valid(gameBoard, y, x)):
			finished = populateBoard(y + 1, gameBoard)
			if(finished):
				return True
		gameBoard[y][x] = 0
	return False

#main

gameBoard = boardGenerator()

ready = populateBoard(0, gameBoard)

print(Board.size)

if(ready):
	printBoard(gameBoard)